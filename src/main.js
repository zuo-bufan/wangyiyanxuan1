import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import 'reset-css'
import '@/vantui'

Vue.config.productionTip = false

// 全局价格过滤器
Vue.filter("RMBformat",val =>{
  return "￥ " + Number(val).toFixed(2) +" 元";
})

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
