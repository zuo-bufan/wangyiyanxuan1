import axios from 'axios';

// 创建实例
const instance = axios.create({
	baseURL: 'http://tech.wolfcode.cn:8001/',
	timeout: 5000
});

// axios  request请求拦截器
instance.interceptors.request.use(
	(config) => {
		console.log('config');
		console.log(config);
		return config;
	},
	(err) => {
		console.log('err');
		console.log(err);
		return Promise.reject(err);
	}
);

// axios response响应拦截器
instance.interceptors.response.use(
	(res) => {
		console.log('res');
		console.log(res.data);

		return res.data;
	},
	(err) => {
		return Promise.reject(err);
	}
);

// 导出此方法
export default instance;
