import request from "./request"

// 首页数据请求
export const GetHomeList = () => request.get("/index/index");

// 获取搜索弹出框数据
export const GetSearchPopupData = () => request.get("/search/index");

// 获取实时搜索的文本提示的数据列表
export const GetSearchTipsListData = (params) => request.get("/search/helper",{params});

// 获取搜索商品的列表数据
export const GetGoodsListData = (params) => request.get("/goods/list",{params});

// 清除历史记录
export const ClearHistory = (params) => request.post("/search/clearhistory",{params});

// 登录接口（没有账号自动注册）  /auth/loginByWeb
export const LoginApi = (params) => request.post('/auth/loginByWeb', params) //post请求时 请求参数params不可以用{}包裹，否则会报错

// 专题页面数据请求
export const GetTopicListData = (params) => request.get('topic/list',{params})

// 全部分类数据接口
export const GetCategoryAllData = () =>request.get('/catalog/index')

// 获取当前的分类数据
export const GetCategoryCurrentData = (params) => request.get('/catalog/current',{params})

// 获取购物车数据 （含完成编辑） /cart/index
export const GetCartDataApi = () => request.get('/cart/index')

// 点击切换商品选中的状态（含全选） /cart/checked
export const GoodsSelectApi = (params) => request.post('/cart/checked',params) //post请求时 请求参数params不可以用{}包裹，否则会报错

// 商品步进器  /cart/update
export const GetStepData = (params) => request.post('/cart/update',params) //post请求时 请求参数params不可以用{}包裹，否则会报错

// 删除商品  /cart/delete
export const DelGoodsData = (params) => request.post('/cart/delete',params) 

// 分类数据获取  /goods/category
export const GoodsCategoryData = (params) => request.get('/goods/category',{params})

// 分类页面商品列表请求  /goods/list
export const GetCategoryGoodsListData =(params) => request.get('/goods/list',{params})

// 产品明细  /goods/details
export const GetGoodsDetailsData = (params) => request.get('/goods/detail',{params}) 

// 相关产品  /goods/related
export const GetAboutProductsData = (params) => request.get('/goods/related',{params})

// 获取购物车产品数量  /cart/goodscount(请求体)
export const CartGoodsNumApi = () => request.get('/cart/goodscount')

// 加入购物车  /cart/add
export const CartAddApi = (params) => request.post('/cart/add', params)

// 品牌详情  /brand/detail
export const GetBrandDetailData = (params) => request.get('brand/detail',{params})

// export const GetProDetailList = () => request.get("/brand/detail");