import Vue from 'vue';
import {
    Button, Search, Swipe, SwipeItem, Icon, Tag, List, Cell,
    DropdownMenu, DropdownItem, Empty, Toast, Tabbar, TabbarItem,
    Col, Row, Grid, GridItem, Form, Field, Pagination, Sidebar, SidebarItem
    , Checkbox, CheckboxGroup, SwipeCell, Card, Stepper, SubmitBar, Tab, Tabs, Popup
    , GoodsAction, GoodsActionIcon, GoodsActionButton, Dialog 
} from 'vant';
Vue.use(Button)
Vue.use(Search);
Vue.use(Swipe);
Vue.use(SwipeItem);
Vue.use(Icon);
Vue.use(Tag);
Vue.use(List);
Vue.use(Cell);
Vue.use(DropdownMenu);
Vue.use(DropdownItem);
Vue.use(Empty);
Vue.use(Toast);
Vue.use(Tabbar);
Vue.use(TabbarItem);
Vue.use(Col);
Vue.use(Row);
Vue.use(Grid);
Vue.use(GridItem);
Vue.use(Form);
Vue.use(Field);
Vue.use(Pagination);
Vue.use(Sidebar);
Vue.use(SidebarItem);
Vue.use(Checkbox);
Vue.use(CheckboxGroup);
Vue.use(SwipeCell);
Vue.use(Card);
Vue.use(Stepper);
Vue.use(SubmitBar);
Vue.use(Tab);
Vue.use(Tabs);
Vue.use(Popup);
Vue.use(GoodsAction);
Vue.use(GoodsActionButton);
Vue.use(GoodsActionIcon);
Vue.use(Dialog);