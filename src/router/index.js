import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    redirect:'/home'
  },
  {
    path: '/home',
    name: 'Home',
    meta:true,
    component: Home,
    children:[
      {
        path: '/home/searchPopup',
        name: 'SearchPopup',
        component: () => import(/* webpackChunkName: "SearchPopup" */ '../views/searchPopup.vue')
      }
    ]
  },
  {
    path: '/topic',
    name: 'Topic',
    meta:true,
    component: () => import(/* webpackChunkName: "SearchPopup" */ '../views/Topic.vue')
  },
  {
    path: '/category',
    name: 'Category',
    meta:true,
    component: () => import(/* webpackChunkName: "SearchPopup" */ '../views/Category.vue')
  },
  {
    path: '/cart',
    name: 'Cart',
    meta:true,
    component: () => import(/* webpackChunkName: "SearchPopup" */ '../views/Cart.vue')
  },
  {
    path: '/user',
    name: 'User',
    meta:true,
    component: () => import(/* webpackChunkName: "SearchPopup" */ '../views/User.vue')
  },
  {
    path: '/channel',
    name: 'Channel',
    component: () => import(/* webpackChunkName: "Channel" */ '../views/Channel.vue')
  },
  {
    path: '/brand',
    name: 'Brand',
    component: () => import(/* webpackChunkName: "Brand" */ '../views/Brand.vue')
  },
  {
    path: '/details',
    name: 'Details',
    component: () => import(/* webpackChunkName: "Details" */ '../views/Details.vue')
  },




 
  // {
  //   path: '/about',
  //   name: 'About',
  //   // route level code-splitting
  //   // this generates a separate chunk (about.[hash].js) for this route
  //   // which is lazy-loaded when the route is visited.
  //   component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  // }
]

const router = new VueRouter({
  mode: 'hash',
  base: process.env.BASE_URL,
  routes
})


// 路由拦截，前置路由守卫
router.beforeEach((to,from,next)=>{
  let token = localStorage.getItem("token");
  if(to.path == "/cart"){
    if(token){
      next();
    }else{
      Vue.prototype.$toast.loading({
        message: "请先登录",
        forbidClick: true
      });
      setTimeout(()=>{
        next("/user");

      },1000)
    }
  }else{

    next();
  }
})

export default router
