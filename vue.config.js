module.exports = {
    assetsDir:'static',
    parallel:false,
    publicPath:'./',
    devServer:{
        port:5000,
        open:true,
    },
}