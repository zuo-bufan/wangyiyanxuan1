# 网易严选（电商）
## 介绍
此项目的2.0版本，改进部分的功能业务，特别适合初学者练手的项目
一个vue框架的网易严选的移动端的电商项目，使用到技术栈有 vue3+vantUI+router+axios+vuex等

vantUI参考文档：https://vant-contrib.gitee
参考教学视频：
https://www.bilibili.com/video/BV16V411q72Q?from=search&sei

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

### 项目展示
#### 1首页
![输入图片说明](https://images.gitee.com/uploads/images/2021/0803/175529_25f6319a_7826448.png "屏幕截图.png")
#### 2.专题
![输入图片说明](https://images.gitee.com/uploads/images/2021/0803/175610_1a657416_7826448.png "屏幕截图.png")
#### 3.分类
![输入图片说明](https://images.gitee.com/uploads/images/2021/0803/175659_83de53cf_7826448.png "屏幕截图.png")
#### 4.购物车
![输入图片说明](https://images.gitee.com/uploads/images/2021/0803/175754_ff138c60_7826448.png "屏幕截图.png")
#### 5.我的个人中心
![输入图片说明](https://images.gitee.com/uploads/images/2021/0803/175839_8302b485_7826448.png "屏幕截图.png")